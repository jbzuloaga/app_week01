import DOMHandler from "../DOMHandler.js";

const inputCalculator = {
  salary: '',
  years: '',
  days: ''
}

const error = {
  salary: true,
  years: true,
  days: true
}

let bonus = '';

const calculator = (function() {
   
  const generateTemplate = function() { 
    return `
      <section>
        <form class='inputs_form'>
          <h1>Bonus Calculator</h1>
          <div class='input_content'>
            <label for='salary'>Salary (in soles)</label>
            <input id='salary' placeholder='Enter your salary here' value=${inputCalculator.salary} >
          </div>
          <div class='input_content'>
            <label for='years'>Years</label>
            <input id='years' placeholder='Enter your years working here' value=${inputCalculator.years} >
          </div>
          <div class='input_content'>
            <label for='days'>Days</label>
            <input id='days' placeholder='Enter your days working here' value=${inputCalculator.days} >
          </div>
          <div>
            ${error.salary ? '' : '<p>Salary must be greater than 0</p>'}
            ${error.years ? '' : '<p>Years must be greater or equal than 0 and less than 35</p>'}
            ${error.days ? '' : '<p>Days must be greater than 0 and less than 365</p>'}
          </div>
          <div class='button_container'>
            <button class='submitButton'>Calculate bonus</button> 
          </div>
        </form>
        <div class='bonus_output'>
          <h2>Bonus</h2>
          <div class='bonus-content'>
            <label for='bonus'>S/.</label>
            <input id='bonus' readonly placeholder='Your bonus here' value='${bonus}' >
          </div>
        </div>
      </section>
    `
  }

  function validations([salary, years, days]) {
    error.salary = salary.value > 0 && salary.value !== '';
    error.years = years.value >= 0 && years.value < 35 && years.value !== '';
    error.days = days.value > 0 && days.value < 365 && days.value !== '';
  }

  function calculateSalaryPerDays(salary, days){
    return (+salary * +days) / 30;
  }

  function calculateBonus({salary, years, days}) {
    if ( +years === 0) {
      return (calculateSalaryPerDays(salary, 15) * +days / 365).toFixed(2);
    } else if (+years >= 1 && +years < 3) {
      return (calculateSalaryPerDays(salary, 15).toFixed(2));
    } else if (+years >= 3 && +years < 10) {
      return (calculateSalaryPerDays(salary, 19).toFixed(2));
    } else {
      return (calculateSalaryPerDays(salary, 21).toFixed(2));
    }
  }

  function listenSubmit() {
    const inputsForm = document.querySelector('.inputs_form');
    const buttonSubmit = document.querySelector('.submitButton');
    const bonusOutput = document.getElementById('bonus');
    inputsForm.addEventListener('submit', (e) => {
      e.preventDefault();
      validations(e.target);
      if (error.salary && error.years && error.days) {
        bonusOutput.value = '';
        buttonSubmit.setAttribute('disabled', 'disabled');
        inputCalculator.salary = e.target[0].value;
        inputCalculator.years = e.target[1].value;
        inputCalculator.days = e.target[2].value;
        setTimeout(() => {
          bonus = calculateBonus(inputCalculator);
          buttonSubmit.removeAttribute('disabled');
          DOMHandler.reload();
        }, 1000);
      } else {
        inputCalculator.salary = error.salary ? e.target[0].value : '';
        inputCalculator.years = error.years ? e.target[1].value : '';
        inputCalculator.days = error.days ? e.target[2].value : '';
        DOMHandler.reload();
      }
    })
  }


  return {
    toString() {
      return generateTemplate()
    },
    addListeners() {
      listenSubmit()
    }
  }
})()

export default calculator