import DOMHandler from "../DOMHandler.js";
import calculator from "./calculatorIndex.js";


function init() {
  DOMHandler.load(calculator);
}

init()