import DOMHandler from "../DOMHandler.js";

let tasks = JSON.parse(localStorage.getItem('tasks')) || [];
let filterTasks = [...tasks];
const filters = {
  byname: '',
  status: '',
  date: ''
}

const todo = (function() {
  let id = 1;
  let nameTask = '';

  function handleId() {
    if (tasks.length !== 0) {
      id = +tasks[tasks.length - 1].id + 1;
    } else {
      id = 1;
    }
    return id;
  }
  
  const renderTask = (task) => {
    return `
      <li class='task'>
        <div class='task_row1'>
          <div class='content'>
            <h6>ID</h6>
            <p>${task.id}</p>
          </div>
          <div class='content'>
            <h6>Task</h6>
            <input class='task_name' id="input-${task.id}" style='border: none;' type='text' value='${task.name}' readonly/>
          </div>
          <div class='content'>
            <h6>Assignee</h6>
            <p>${task.assignee}</p>
          </div>
          <div class='content' style='width: 100%;'>
            <h6>Status</h6>
            <p>${task.status}</p>
          </div>
        </div>
        <div class='task_row2'>
          <div class='content'>
            <h6>Creation Date</h6>
            <p>${task.date}</p>
          </div>
          <div class='task_actions'>
            <button class='edit' data-id=${task.id}>Edit</button>
            <button class='delete' data-id=${task.id}>Delete</button>
          </div>
        </div>
      </li>
    `
  }

  const generateTemplate = function() { 
    return `
    <div class='wrapper'>
      <header>
        <h1>New Task</h1>
        <form class='new-task-form'>
          <div class='new-task-content'>
            <div class='input_name'>
              <h3>Name</h3>
              <div class='new-task-input'>
                <span id='taskId' data-id=${id}>${handleId()}</span>
                <input
                  type="text"
                  id="nameTask"
                  placeholder="Enter your task here"
                  value=${nameTask}
                >
              </div>
            </div>
            <div class='input_assignee'>
              <h3>Assignee</h3>
              <select class="selectAssignee minimal" name="Assignee" id="assignee">
                <option value="Frank">Frank</option>
                <option value="John">John</option>
                <option value="Alice">Alice</option>
                <option value="Mary">Mary</option>
              </select>
            </div>
            <div class='statusCheckboxes'>
              <h3>Status</h3>
              <label class='labelCheckbox'
                ><input type="checkbox" class='cbox' id="cbox1" value="pending" checked/> Pending</label
              >
              <label class='labelCheckbox'><input type="checkbox" class='cbox' id="cbox2" value="done"/> Done</label
              >
            </div>
          </div>
          <div class='new-task-button'>
            <button id='submitButtonForm' type='submit'>Submit</button>
          </div>
        </form>
      </header>
      <div class='aside_container'>
        <div class='filter_container'>
          <h2>Filters</h2>
          <div class='filter_container_actions'>
            <form class='searchByTask filter_row1'>
              <h6>Search by name</h6>
              <input id='filterName' type='text' placeholder='Press enter to search' value=${filters.byname} >
            </form>
            <div class='filter_row2'>
              <form class='searchByStatus'>
                <h6>Sort by status</h6>
                <label
                ><input type="checkbox" class='cbox filterCbox' id="cbox3" value="pending" ${filters.status === 'pending' ? 'checked' : ''} /> Pending</label
                >
                <label><input type="checkbox" class='cbox filterCbox' id="cbox4" value="done" ${filters.status === 'done' ? 'checked' : ''} /> Done</label
                ><br />
              </form>
              <form class='sortByDate'>
                <h6>Sort by date</h6>
                <select name="sortDate" id="sortDate">
                  <option disabled ${filters.date === '' ? 'selected' : ''}>Select an option</option>
                  <option value="ascendant" ${filters.date === 'ascendant' ? 'selected' : ''}>ascendant</option>
                  <option value="descendant" ${filters.date === 'descendant' ? 'selected' : ''}>desendant</option>
                </select>
              </form>
            </div>
          </div>
        </div>
        <section>
          <h2>List Tasks</h2>
          <ul class='task-list'>
            ${filterTasks.length === 0 ? '<p>no task to show</p>': filterTasks.map(task => renderTask(task)).join('')}
          </ul>
        </section>
      </div>
    </div>
    `
  }

  function handleCheckboxes() {
    const checkboxes = document.querySelectorAll('.cbox');
    checkboxes.forEach((element) => {
      element.addEventListener('change', (e) => {
        if (e.target.id === 'cbox1' && e.target.checked) {
          checkboxes[1].checked = false;
          return
        }
        if (e.target.id === 'cbox2' && e.target.checked) {
          checkboxes[0].checked = false;
        }
        if (e.target.id === 'cbox3' && e.target.checked) {
          checkboxes[3].checked = false;
        }
        if (e.target.id === 'cbox4' && e.target.checked) {
          checkboxes[2].checked = false;
        }
      })
    })
  }

  function listenSubmit() {
    const inputSubmit = document.querySelector('.new-task-form');
    const buttonSubmit = document.querySelector('#submitButtonForm');
    inputSubmit.addEventListener('submit',(e) => {
      e.preventDefault();
      if (e.target[1].value.length < 100 && e.target[1].value !== '') {
        buttonSubmit.setAttribute('disabled', 'disabled');
        setTimeout(() => {
          const task = {
            id: id,
            name: e.target[0].value,
            assignee: e.target[1].value,
            status: e.target[2].checked ? e.target[2].value : e.target[3].value,
            date: new Date().toLocaleDateString("en-US"),
          }
          filterTasks.push(task);
          tasks.push(task);
          localStorage.setItem('tasks', JSON.stringify(filterTasks));
          Object.keys(filters).forEach((key) => filters[key] = '')
          buttonSubmit.removeAttribute('disabled');
          DOMHandler.reload();
        }, 1000);
      } else {
        console.log('error');
        alert("Name must contain a maximum of 100 characters and can't be empty");
      }
    })

  }

  function listenDelete() {
    const deleteButton = document.querySelectorAll('.delete');
    deleteButton.forEach( element => {
      element.addEventListener('click', (e) => {
        filterTasks = filterTasks.filter((el) => el.id !== +e.target.dataset.id )
        tasks = tasks.filter((el) => el.id !== +e.target.dataset.id )
        localStorage.setItem('tasks', JSON.stringify(tasks));
        DOMHandler.reload();
      })
    })
  }

  function listenEdit() {
    const editButton = document.querySelectorAll('.edit');
    editButton.forEach( element => {
      element.addEventListener('click', (e) => {
        const inputValue = document.getElementById(`input-${e.target.dataset.id}`)
        if (element.innerText.toLowerCase() === 'edit') {
          element.innerText = 'Save';
          inputValue.removeAttribute('readonly');
          inputValue.focus();
        } else {
          inputValue.setAttribute("readonly","readonly");
          element.innerText = 'Edit';
          const task = filterTasks.find((el) => el.id === e.target.dataset.id )
          task.name = inputValue.value
          localStorage.setItem('tasks', JSON.stringify(tasks))
          DOMHandler.reload();
        }
      })
    })
  }

  function listenSearchName() {
    const searchText = document.querySelector('.searchByTask');
    searchText.addEventListener('submit', (e) => {
      e.preventDefault();
      if (e.target[0].value.length === 0) {
        filterTasks = filters.status !== '' ? tasks.filter((task) => task.status === filters.status) : tasks;
        filters.byname = '';
      } else {
        filters.byname = e.target[0].value;
        filterTasks = filters.status !== '' || filters.date !== '' ? filterTasks.filter((task) => task.name.includes(e.target[0].value)) : tasks.filter((task) => task.name.includes(e.target[0].value));
      }
      DOMHandler.reload();
    })
  }

  function listenSearchStatus() {
    const searchStatus = document.querySelectorAll('.filterCbox');
    searchStatus.forEach((element) => {
      element.addEventListener('change', (e) => {
        if (e.target.checked) {
          filterTasks = tasks.filter((task) => task.status === e.target.value)
          filters.status = e.target.value;
        } else {
          filters.status = ''
          filterTasks = tasks;
        }
        DOMHandler.reload();
      })
    })
  }

  function listenSortByDate() {
    const sortDate = document.querySelector('.sortByDate');
    sortDate.addEventListener('change', (e) => {
      switch (e.target.value) {
        case 'ascendant':
          filterTasks = tasks.sort((a, b) => new Date(a.date) - new Date(b.date));
          filters.date = 'ascendant';
          break;
        case 'descendant':
          filterTasks = tasks.sort((a, b) => new Date(b.date) - new Date(a.date));
          filters.date = 'descendant';
          break;
      }
      DOMHandler.reload();
    })
  }

  return {
    toString() {
      return generateTemplate()
    },
    addListeners() {
      listenSubmit(),
      listenDelete(),
      listenEdit(),
      handleCheckboxes(),
      listenSearchName(),
      listenSearchStatus(),
      listenSortByDate()
    }
  }
})()

export default todo